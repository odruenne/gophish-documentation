# Gophish

## RESSOURCES 

	https://getgophish.com/

	https://getgophish.com/documentation/

	https://github.com/gophish/gophish/issues

	https://github.com/FreeZeroDays/GoPhish-Templates/tree/master

	https://www.youtube.com/watch?v=CQi6O92FtcE


## DÉFINITIONS

	Phishing : Le phishing (ou hameçonnage en français) est une technique utilisée par 
	des fraudeurs afin de récupérer des informations personnelles 
	(nom, prénom, numéro de sécurité sociale, coordonnées bancaires), 
	dans le but d'usurper l'identité de l'utilisateur ciblé.

	Le fraudeur va envoyer un e-mail en se faisant passer par un tiers connu de la victime 
	(banque, sécurité sociale, administration), en créant une copie exacte du site 
	internet original, afin que la victime ne se doute de rien.

	Protocole SMTP : Le protocole Simple Mail Transfer Protocol 
	(SMTP, ou Protocole Simple de Transfert de Courrier en français), 
	est un protocole de communication utilisé pour transférer le courriel 
	vers les serveurs de messagerie électronique.

	Voici les différentes étapes du protocole SMTP :

	1.   Spécifier l'expéditeur du message
	2.   Spécifier le ou les destinataires du message (leurs existences sont préalablement vérifiées).
	3.  Transfert du corps de message.

	Quelques abréviations à connaître :

	-   MUA (Mail User Agent) : client de messagerie (par exemple Gmail ou Outlook)
	-   MTA (Mail Transfer Agent) : logiciel qui reçoit les mails d’un MUA sur un serveur de transmission
	-   MDA (Mail Delivery Agent) : logiciel qui stocke les messages sur la boîte de réception des destinataires

	Ci-dessous, vous pouvez trouver un schéma explicatif du protocole SMTP :

![Protocole SMTP](https://1.bp.blogspot.com/-mJalpSnTW3Q/XP10eCn0tPI/AAAAAAAAEBY/CuNWB-o6hZUJcp-_F3E7oDnU5ED5O1vwQCLcBGAs/s1600/smtp.png)
## GOPHISH

![GOPHISH](https://camo.githubusercontent.com/e401fbe8f27b84bdba6d577b6ba7c693029491d2a93b86662c31e16f9cef6712/68747470733a2f2f7261772e6769746875622e636f6d2f676f70686973682f676f70686973682f6d61737465722f7374617469632f696d616765732f676f70686973685f707572706c652e706e67)


Gophish est un outil qui aident les entreprises à effectuer des simulations d'hameçonnage.

L'outil se veut abordable (il est gratuit, alors que d'autres librairies comme Phisher de KnowBe-4 et Darktrace sont payantes) et accessible : il est écrit en Go. Cela permet d'avoir des nouvelles versions compilées et sans dépendances.


## INSTALLATIONS 

### INSTALLATION COMPLÈTE 
Gophish étant écrit en Go, il faut d'abord posséder la dernière version de Go sur votre ordinateur. Si ce n'est pas le cas, vous pouvez installer Go sur Linux, Mac et Windows. Vous pouvez trouver toutes les étapes en cliquant [ici](https://go.dev/doc/install)

Cette documentation pour installer Go sur Ubuntu/Linux est également très bien : [cliquez-ici](https://gophercoding.com/install-latest-version-of-go/)

Pour lancer l'installation de Gophish, entrez cette commande dans un terminal :

	$ go install github.com/gophish/gophish@latest

Puis, allez dans votre dossier gophish ( ~/go/pkg/mod/github.com/gophish/gophish@v0.12.1 ) et lancez la commande suivante :

	$ sudo go build

Dans le fichier config.json , vous pouvez modifier les options selon vos préférences :



![config.json](https://i.ibb.co/CbDxfJZ/config.png)


## CONFIGURATION

### CERTIFICAT SSL

Si vous souhaitez générer le certificat SSL ainsi que la clé privée, vous pouvez les générer avec la commande suivante (pensez à vérifier que vous avez déjà openssl d'installé sur votre machine) :

	$ openssl req -newkey rsa:2048 -nodes -keyout gophish.key -x509 -days 365 -out gophish.crt

### BASE DE DONNÉES 
La base de données par défaut de Gophish est SQLite. Cependant, il est possible d'utiliser MySQL. Pour cela, vous devez :

-   Configurer le fichier config.json en remplaçant les lignes que vous avez par celles-ci :

		"db_name" : "mysql",
		"db_path" : "root:@(:3306)/gophish?charset=utf8&parseTime=True&loc=UTC",

Le format pour la ligne db_path est le suivant :

	username:password@(host:port)/database?charset=utf8&parseTime=True&loc=UTCusername:password@(host:port)/database?charset=utf8&parseTime=True&loc=UTC

Enfin, Gophish utilise un format de date qui est incompatible avec une version de MySQL >= 5.7

Pour modifier cela, allez dans le fichier :

	/etc/mysql/mysql.cnf/etc/mysql/mysql.cnf

Et ajoutez à la fin du fichier :

	[mysqld]
	sql_mode=ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION

Enfin, il vous faudra créer une base de données. Pour cela, connectez-vous à MySQL et exécutez la commande suivante :

	CREATE DATABASE gophish

	CHARACTER SET utf8mb4

	COLLATE utf8mb4_unicode_ci;

### INSTALLATION RAPIDE

Pour commencer un projet Gophish à partir du répertoire GitHub, il suffit de lancer la commande suivante :
		
	$ git clone https://github.com/gophish/gophish.git

Puis, allez dans le répertoire source du projet avec la commande **cd/** et exécutez la commande **go build.**
Vous devriez voir apparaitre un fichier gophish.


## EXÉCUTION 

Pour exécuter le programme, ouvrez un terminal et allez dans le dossier ou le fichier gophish binaire est situé, et exécutez-le :

	$ sudo ./gophish

Puis, allez sur l'URL suivante : https://127.0.0.1:3333/

Connectez-vous avec le login **admin**, et avec le **mot de passe** qui est inscrit dans le terminal :

![loginpwd](https://i.ibb.co/RQKN63Y/login.png)


Gophish vous demandera de changer le mot de passe.

Enfin, si toutes les étapes ci-dessus se sont bien déroulées, vous devriez arriver sur cette page :

![dashboard](https://i.ibb.co/SndCsgZ/dashboard.png)



Voici les différents onglets que l'on peut retrouver sur Gophish ainsi que leurs descriptions : 

**Dashboard** : Tableau de bord.

**Campaigns** : Permet de voir les campagnes archivées ainsi que les campagnes actives. 

**Users & Groups** : Permet d'enregistrer des utilisateurs ainsi que de créer des groupes. 

**Email Templates** : Permet de créer les templates des mails.

**Landing Pages** : Permet de créer les templates des sites internet utilisés pour le phishing. 

**Sending Profiles** : Permet de créer de faux profils. 

**Account Settings** : Permet de gérer les paramètres du compte (mot de passe, clé d'API, IMAP et résultats des campagnes).

**User Management** : Permet de gérer les utilisateurs.

**Webhooks** : Permet de créer des Webhooks.


## CRÉATION D'UNE CAMPAGNE 

### SENDING PROFILE 
(Dans la capture d'écran, l'Host va correspondre à l'adresse du serveur SMTP de MailDev)


![MailDev](https://i.ibb.co/N3pPrW0/sending.png)


![EditSendingProfile](https://i.ibb.co/G3QQTgZ/editsending.png)


Pour créer le profil d'envoi de mails de l'IRCAD, voici à quoi correspondent les champs du formulaire : 

**SMTP From :** correspond à une adresse mail qui existe

**SMTP Host** : Toujours la même chose.

**Username :** Même adresse mail que le SMTP From.

**Password :** Correspond au mot de passe d'application (méthode d'authentification d'office)

Exemple : 

![SendingProfileIRCAD](https://i.ibb.co/4VH14dC/Capture-d-cran-du-2023-06-21-14-55-25.png)



### LANDING PAGES 

![LandingPage](https://i.ibb.co/sQjkrS6/landingpage.png)


Le lien de redirection correspond à la page qui s'affichera une fois que l'utilisateur aura entré ses données dans le formulaire.

Concernant **l'Envelope Sender,** l'adresse mail correspond à l'alias défini dans Office (que l'on peut changer à l'infini)

Exemple : 

![EnvelopeSenderIRCAD](https://i.ibb.co/rmbkTrM/Capture-d-cran-du-2023-06-21-14-57-40.png)

### FORMULAIRE
![Form1](https://i.ibb.co/WFvgdNv/form1.png)


![Form2](https://i.ibb.co/SRsHXMn/form2.png)

### EMAIL TEMPLATE


![EmailTemplate](https://i.ibb.co/mXC2CMB/ircad-mail.png)


Il est possible d'importer un email en cliquant sur le bouton rouge Import Email. Pour récupérer un email en format brut dans GMAIL, il suffit de cliquer sur un mail, et de cliquer sur les trois petits points en haut à droite "Afficher l'original"


### USERS & GROUPS
Il est possible d'importer un fichier CSV de contacts à condition que celui-ci respecte la forme suivante :

![CSV](https://i.ibb.co/DD5stBr/Capture-d-cran-du-2023-05-09-15-37-01.png)


![USERSGROUPS](https://i.ibb.co/XZhLW4L/GROUP.png)


## CAMPAGNE

Pour trouver l'URL que vous devez indiquer, il faut effectuer la commande : 
				
	$ ip address 
	
et prendre l'adresse commençant par **192.168.X.XXX** (**Attention à bien indiquer le http:// devant**)

![Campagne](https://i.ibb.co/8ccGtRV/campagne.png)


## MAILDEV

![MAILDEV](https://i.ibb.co/M5SLxMd/Capture-d-cran-du-2023-05-11-10-02-23.png)


En cliquant sur "Répondre sur Indeed", le formulaire va s'afficher, et à la validation de celui-ci, l'utilisateur sera redirigé vers Google.


## RÉSULTATS

![RESULT1](https://i.ibb.co/RPwmRpY/result1.png)


![RESULT2](https://i.ibb.co/cxjbp5d/result2.png)



## CONFIGURER SMTP POUR GMAIL 

Cette vidéo explique très bien comment configurer SMTP pour GMAIL : 

	https://youtu.be/QR3Pp1D1y2I


1. Allez sur la page [Se connecter avec des mots de passe d'application de Google](https://support.google.com/accounts/answer/185833?hl=fr&sjid=11453461992842770256-EU)


2. Allez sur la page [Mon compte Google](https://myaccount.google.com/)


3. Allez sur [Mots de passe des applications](https://myaccount.google.com/apppasswords?pli=1&rapt=AEjHL4M0nrvsGS6xFxH-mhv_SDDudzlNyIk1juN4ALvY7lAz4pL5WyUJ146gSV2dqLrfTT7TYFUT1dDOgpuCVKth3csfBAKl-g)


4. Configurez votre SMTP de la manière suivante : 
![Mots de passe des applis](https://i.ibb.co/gyJt4c5/mdp.png)

(Cliquez sur Autre) et inscrivez le nom "Gophish" 

![Gophish](https://i.ibb.co/BC9yRNk/gophish.png)

Et cliquez sur GÉNÉRER 


5. Votre mot de passe pour SMTP est généré. 

![mot_de_passe_smtp](https://i.ibb.co/BGDS5fB/genere.png)


6. Pour l'utiliser dans Gophish, allez dans Sending Profiles, et créez un nouveau Sending Profile de la manière suivante : 

![configGophish](https://i.ibb.co/6FFDZvk/configg.png)

Le champ SMTP From correspond à l'adresse mail GMAIL qui sera utilisée pour le SMTP. 

L'Host sera toujours le même : 

	smtp.gmail.com:465


L'Username correspond à la même adresse GMAIL utilisée précédemment.

Le password correspond au mot de passe d'application généré plus haut. 



#### NOTES 

Si, en exécutant la commande ./sudo gophish, ce message d'erreur s'affiche dans votre console : 

	time="2023-05-15T08:50:21+02:00" level=warning msg="No 	contact address has been configured."
	time="2023-05-15T08:50:21+02:00" level=warning msg="Please consider adding a contact_address entry in your config.json"
	goose: no migrations to run. current version: 20220321133237
	time="2023-05-15T08:50:21+02:00" level=info msg="Starting admin server at https://127.0.0.1:3333"
	time="2023-05-15T08:50:21+02:00" level=info msg="Starting phishing server at http://0.0.0.0:80"
	time="2023-05-15T08:50:21+02:00" level=fatal msg="listen tcp 0.0.0.0:80: bind: address already in use".

Pour fermer tous les serveurs lancés, vous pouvez effectuer la commande : 

	$ sudo systemctl stop apache2.service

Et relancer le serveur avec la commande : 

	$ sudo ./gophish

Commande pour lancer MailDev : 

	$ docker run -p 1080:1080 -p 1025:1025 maildev/maildev


Procédure pour modifier le nom du compte expéditeur du mail : 
1. Se connecter au centre d’administration Exchange, sélectionner l’utilisateur et choisir ‘Modifier’ / ‘Informations de contact’ :

![modifier](https://i.ibb.co/z8QpWbG/1.png)
![modifier2](https://i.ibb.co/ZLY0MPS/2.png)

2. Modifier les champs en fonction du nouveau nom à donner : 
![nouveau_nom](https://i.ibb.co/pQNmbp2/3.png)

3. Retourner sur la fiche de l’utilisateur du centre d’administration Exchange, onglet général, sélectionner ‘Gérer les types d’adresses de courrier’ puis ‘Ajouter un type d’adresse de courrier’ : 
![fiche](https://i.ibb.co/bB8bF67/4-Sans-titre.png)

![fiche2](https://i.ibb.co/BwcL00v/S5ans-titre.png)

4. Saisir le nouvel alias associé au compte, et cocher la case ‘Définir comme adresse de courrier principale’ : 
![alias](https://i.ibb.co/xzXg0Hz/thumbnail-image006.png)
